package bank;

import bank.*;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;

public class bank
{
	private String name;
	private int lastaccno;
	private Map<Integer,account> accountMap = new HashMap<Integer,account>();
	
	private bank(String n, int bankCode){
		this.name=n;
		this.lastaccno=bankCode*10000;
		}
		
	public String getName(){
		return this.name;
		}
		
	public int getBankCode(){
		return this.lastaccno/10000;
		}
		
/*	public int openSavingsAccount(String n, long openBal){
		account ac=new saving(++lastaccno,n,openBal);
		accountMap.put(ac.getAccountNo(),ac);
		return ac.getAccountNo;
		}
*/		
	public int openCurrentAccount(String n, long openBal){
		account ac=new current(++lastaccno,n,openBal);
		accountMap.put(ac.getAccountNo(),ac);
		return ac.getAccountNo();
		}
		
	private account getAccount(int acno){
		account ac = accountMap.get(acno);
		return ac;
		}
		
	
	public void deposit(int acno,long amt){
		 getAccount(acno).deposit(amt);
		 }
		 
	public boolean withdraw(int acno, long amt){
		return getAccount(acno).with(amt);
		}
		
	public void display(int acno){
		getAccount(acno).display();
		}
		
	public void printpassbook(int acno){
		getAccount(acno).printpassbook();
		}
		
	public void listAccounts(){
		Collection <account>accounts= accountMap.values();
		System.out.println("account list for bank: "+name);
		for (account ac : accounts){
			System.out.println(ac);
		}
		System.out.println("End Of Account list");
	}
	}
